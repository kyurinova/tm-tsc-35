package ru.tsc.kyurinova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.repository.ISessionRepository;
import ru.tsc.kyurinova.tm.api.service.ISessionService;
import ru.tsc.kyurinova.tm.endpoint.Session;

public class SessionService implements ISessionService {

    private final ISessionRepository sessionRepository;

    public SessionService(@NotNull final ISessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    @Override
    @Nullable
    public Session getSession() {
        return sessionRepository.getSession();
    }

    @Override
    public void setSession(@Nullable final Session session) {
        sessionRepository.setSession(session);
    }


}
