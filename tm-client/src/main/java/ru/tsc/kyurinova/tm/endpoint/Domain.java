
package ru.tsc.kyurinova.tm.endpoint;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for domain complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="domain"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="users" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="users" type="{http://endpoint.tm.kyurinova.tsc.ru/}user" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="projects" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="projects" type="{http://endpoint.tm.kyurinova.tsc.ru/}project" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="tasks" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="tasks" type="{http://endpoint.tm.kyurinova.tsc.ru/}task" maxOccurs="unbounded" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "domain", propOrder = {
        "users",
        "projects",
        "tasks"
})
public class Domain {

    protected Domain.Users users;
    protected Domain.Projects projects;
    protected Domain.Tasks tasks;

    /**
     * Gets the value of the users property.
     *
     * @return possible object is
     * {@link Domain.Users }
     */
    public Domain.Users getUsers() {
        return users;
    }

    /**
     * Sets the value of the users property.
     *
     * @param value allowed object is
     *              {@link Domain.Users }
     */
    public void setUsers(Domain.Users value) {
        this.users = value;
    }

    /**
     * Gets the value of the projects property.
     *
     * @return possible object is
     * {@link Domain.Projects }
     */
    public Domain.Projects getProjects() {
        return projects;
    }

    /**
     * Sets the value of the projects property.
     *
     * @param value allowed object is
     *              {@link Domain.Projects }
     */
    public void setProjects(Domain.Projects value) {
        this.projects = value;
    }

    /**
     * Gets the value of the tasks property.
     *
     * @return possible object is
     * {@link Domain.Tasks }
     */
    public Domain.Tasks getTasks() {
        return tasks;
    }

    /**
     * Sets the value of the tasks property.
     *
     * @param value allowed object is
     *              {@link Domain.Tasks }
     */
    public void setTasks(Domain.Tasks value) {
        this.tasks = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="projects" type="{http://endpoint.tm.kyurinova.tsc.ru/}project" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "projects"
    })
    public static class Projects {

        protected List<Project> projects;

        /**
         * Gets the value of the projects property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the projects property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getProjects().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Project }
         */
        public List<Project> getProjects() {
            if (projects == null) {
                projects = new ArrayList<Project>();
            }
            return this.projects;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="tasks" type="{http://endpoint.tm.kyurinova.tsc.ru/}task" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "tasks"
    })
    public static class Tasks {

        protected List<Task> tasks;

        /**
         * Gets the value of the tasks property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the tasks property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getTasks().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Task }
         */
        public List<Task> getTasks() {
            if (tasks == null) {
                tasks = new ArrayList<Task>();
            }
            return this.tasks;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="users" type="{http://endpoint.tm.kyurinova.tsc.ru/}user" maxOccurs="unbounded" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "users"
    })
    public static class Users {

        protected List<User> users;

        /**
         * Gets the value of the users property.
         *
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the users property.
         *
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getUsers().add(newItem);
         * </pre>
         *
         *
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link User }
         */
        public List<User> getUsers() {
            if (users == null) {
                users = new ArrayList<User>();
            }
            return this.users;
        }

    }

}
