package ru.tsc.kyurinova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractCommand;
import ru.tsc.kyurinova.tm.endpoint.Session;

public class DataYamlLoadFasterXMLCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-yaml-load";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load yaml data from file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA YAML LOAD]");
        @Nullable final Session session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminDataEndpoint().dataYamlLoadFasterXML(session);
    }

}
