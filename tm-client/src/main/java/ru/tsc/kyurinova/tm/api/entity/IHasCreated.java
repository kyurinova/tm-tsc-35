package ru.tsc.kyurinova.tm.api.entity;

import java.util.Date;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IHasCreated {

    @Nullable Date getCreated();

    void setCreated(@NotNull Date created);

}
