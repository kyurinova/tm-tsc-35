package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.endpoint.Session;

public interface ISessionService {

    @Nullable
    Session getSession();

    void setSession(@Nullable final Session session);


}
