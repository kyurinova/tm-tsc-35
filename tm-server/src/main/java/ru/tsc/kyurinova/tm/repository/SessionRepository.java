package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.api.repository.ISessionRepository;
import ru.tsc.kyurinova.tm.model.Session;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public boolean exists(@NotNull final String id) {
        return entities.stream().anyMatch(e -> id.equals(e.getId()));
    }

}
