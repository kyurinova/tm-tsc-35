package ru.tsc.kyurinova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.kyurinova.tm.api.service.IProjectTaskService;
import ru.tsc.kyurinova.tm.api.service.ISessionService;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.Session;
import ru.tsc.kyurinova.tm.model.Task;
import ru.tsc.kyurinova.tm.service.ProjectTaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectTaskEndpoint implements IProjectTaskEndpoint {

    private ISessionService sessionService;

    private IProjectTaskService projectTaskService;

    public ProjectTaskEndpoint(ISessionService sessionService, IProjectTaskService projectTaskService) {
        this.sessionService = sessionService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    @WebMethod
    public @Nullable Task bindTaskById(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId,
            @Nullable
            @WebParam(name = "taskId", partName = "taskId")
                    String taskId
    ) {
        sessionService.validate(session);
        return projectTaskService.bindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @WebMethod
    public @Nullable Task unbindTaskById(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId,
            @Nullable
            @WebParam(name = "taskId", partName = "taskId")
                    String taskId
    ) {
        sessionService.validate(session);
        return projectTaskService.unbindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @NotNull
    @WebMethod
    public void removeAllTaskByProjectId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId
    ) {
        sessionService.validate(session);
        projectTaskService.removeAllTaskByProjectId(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public @Nullable Project removeById(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId
    ) {
        sessionService.validate(session);
        return projectTaskService.removeById(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public @NotNull List<Task> findAllTaskByProjectId(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    Session session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId
    ) {
        sessionService.validate(session);
        return projectTaskService.findAllTaskByProjectId(session.getUserId(), projectId);
    }
}
