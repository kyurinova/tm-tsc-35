package ru.tsc.kyurinova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.kyurinova.tm.api.repository.IUserRepository;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.api.service.IUserService;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.repository.UserRepository;
import ru.tsc.kyurinova.tm.util.HashUtil;

public class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final User user;

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userTest";

    @NotNull
    private final String userEmail = "test@mail.com";

    public UserServiceTest() {
        user = new User();
        userId = user.getId();
        user.setLogin(userLogin);
        user.setEmail(userEmail);
        user.setPasswordHash(HashUtil.salt("test", 3, "test"));
        @NotNull final IUserRepository userRepository = new UserRepository();
        userRepository.add(user);
        propertyService = new PropertyService();
        userService = new UserService(userRepository, propertyService);
    }

    @Test
    public void findByUserTest() {
        Assert.assertEquals(user, userService.findById(userId));
        Assert.assertEquals(user, userService.findByIndex(0));
        Assert.assertEquals(user, userService.findByLogin(userLogin));
        Assert.assertEquals(user, userService.findByEmail(userEmail));
    }

    @Test
    public void removeUserByIdTest() {
        Assert.assertNotNull(userId);
        userService.removeById(userId);
        Assert.assertNull(userService.findById(userId));
    }

    @Test
    public void removeUserByLoginTest() {
        Assert.assertNotNull(userLogin);
        userService.removeByLogin(userLogin);
        Assert.assertNull(userService.findByLogin(userLogin));
    }

    @Test
    public void existsUserByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertTrue(userService.existsById(userId));
    }

    @Test
    public void isLoginExistsTest() {
        Assert.assertNotNull(userLogin);
        Assert.assertTrue(userService.isLoginExists(userLogin));
    }

    @Test
    public void isEmailExistsTest() {
        Assert.assertNotNull(userEmail);
        Assert.assertTrue(userService.isEmailExists(userEmail));
    }

    @Test
    public void createNoEmailTest() {
        Assert.assertEquals(1, userService.findAll().size());
        @NotNull final String newUserLogin = "newUserTest";
        @NotNull final String newUserPassword = "newPassword";
        @NotNull final User newUser = userService.create(newUserLogin, newUserPassword);
        Assert.assertEquals(newUserLogin, newUser.getLogin());
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        Assert.assertEquals(HashUtil.salt(secret, iteration, newUserPassword), newUser.getPasswordHash());
        Assert.assertEquals(2, userService.findAll().size());
    }

    @Test
    public void createTest() {
        Assert.assertEquals(1, userService.findAll().size());
        @NotNull final String newUserLogin = "newUserTest";
        @NotNull final String newUserEmail = "newTest@mail.com";
        @NotNull final String newUserPassword = "newPassword";
        @NotNull final User newUser = userService.create(newUserLogin, newUserPassword, newUserEmail);
        Assert.assertEquals(newUserLogin, newUser.getLogin());
        Assert.assertEquals(newUserEmail, newUser.getEmail());
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        Assert.assertEquals(HashUtil.salt(secret, iteration, newUserPassword), newUser.getPasswordHash());
        Assert.assertEquals(2, userService.findAll().size());
    }

    @Test
    public void setPasswordTest() {
        Assert.assertEquals(user.getPasswordHash(), HashUtil.salt("test", 3, "test"));
        @NotNull final String newPassword = "newPassword";
        userService.setPassword(userId, newPassword);
        Assert.assertNotEquals(HashUtil.salt("test", 3, "test"), user.getPasswordHash());
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        Assert.assertEquals(HashUtil.salt(secret, iteration, newPassword), user.getPasswordHash());
    }

    @Test
    public void updateUserTest() {
        @NotNull final String newFirstName = "newFirstNameTest";
        @NotNull final String newLastName = "newLastNameTest";
        @NotNull final String newMiddleName = "newMiddleNameTest";
        Assert.assertNull(user.getLastName());
        Assert.assertNull(user.getFirstName());
        Assert.assertNull(user.getMiddleName());
        userService.updateUser(userId, newFirstName, newLastName, newMiddleName);
        Assert.assertEquals(newFirstName, user.getFirstName());
        Assert.assertEquals(newLastName, user.getLastName());
        Assert.assertEquals(newMiddleName, user.getMiddleName());
    }

    @Test
    public void lockUnlockByLoginTest() {
        Assert.assertFalse(user.getLocked());
        userService.lockUserByLogin(userLogin);
        Assert.assertTrue(user.getLocked());
        userService.unlockUserByLogin(userLogin);
        Assert.assertFalse(user.getLocked());
    }

}
