package ru.tsc.kyurinova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.tsc.kyurinova.tm.api.repository.IProjectRepository;
import ru.tsc.kyurinova.tm.api.service.IProjectService;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.repository.ProjectRepository;
import ru.tsc.kyurinova.tm.util.HashUtil;

public class ProjectServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String projectName = "testProject";

    @NotNull
    private final String projectDescription = "Description ror project";

    @NotNull
    private final String userId;

    public ProjectServiceTest() {
        @NotNull User user = new User();
        userId = user.getId();
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt("test", 3, "test"));
        project = new Project();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName(projectName);
        project.setDescription(projectDescription);
        projectRepository.add(userId, project);
        projectService = new ProjectService(projectRepository);
    }

    @Test
    public void createByNameTest() {
        @NotNull final String newProjectName = "newTestProject";
        projectService.create(userId, newProjectName);
        Assert.assertEquals(2, projectService.findAll().size());
        Assert.assertNotNull(projectService.findByName(userId, newProjectName));
    }

    @Test
    public void createTest() {
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        projectService.create(userId, newProjectName, newProjectDescription);
        Assert.assertEquals(2, projectService.findAll().size());
        Assert.assertNotNull(projectService.findByName(userId, newProjectName));
        Assert.assertEquals(newProjectDescription, projectService.findByName(userId, newProjectName).getDescription());
    }

    @Test
    public void findByProjectTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectName);
        Assert.assertEquals(project, projectService.findById(userId, projectId));
        Assert.assertEquals(project, projectService.findByIndex(userId, 0));
        Assert.assertEquals(project, projectService.findByName(userId, projectName));
    }

    @Test
    public void existsProjectTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertTrue(projectService.existsById(userId, projectId));
        Assert.assertTrue(projectService.existsByIndex(userId, 0));
    }

    @Test
    public void updateByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectDescription);
        Assert.assertEquals(project, projectService.findByName(userId, projectName));
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        projectService.updateById(userId, projectId, newProjectName, newProjectDescription);
        Assert.assertEquals(project, projectService.findByName(userId, newProjectName));
        @NotNull final Project newProject = projectService.findByName(userId, newProjectName);
        Assert.assertEquals(userId, newProject.getUserId());
        Assert.assertEquals(projectId, newProject.getId());
        Assert.assertEquals(newProjectName, newProject.getName());
        Assert.assertEquals(newProjectDescription, newProject.getDescription());
    }

    @Test
    public void updateByIndexTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectDescription);
        Assert.assertEquals(project, projectService.findById(userId, projectId));
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        projectService.updateByIndex(userId, 0, newProjectName, newProjectDescription);
        Assert.assertEquals(project, projectService.findByIndex(userId, 0));
        @NotNull final Project newProject = projectService.findByIndex(userId, 0);
        Assert.assertEquals(userId, newProject.getUserId());
        Assert.assertEquals(projectId, newProject.getId());
        Assert.assertEquals(newProjectName, newProject.getName());
        Assert.assertEquals(newProjectDescription, newProject.getDescription());
    }

    @Test
    public void removeProjectByIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.removeById(userId, projectId);
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    public void removeProjectByIndexTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        projectService.removeByIndex(userId, 0);
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    public void removeProjectByNameTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.removeByName(userId, projectName);
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    public void startByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.startById(userId, projectId);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findById(userId, projectId).getStatus());
    }

    @Test
    public void startByIndexTest() {
        Assert.assertNotNull(userId);
        projectService.startByIndex(userId, 0);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void startByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.startByName(userId, projectName);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByName(userId, projectName).getStatus());
    }

    @Test
    public void finishByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.finishById(userId, projectId);
        Assert.assertEquals(Status.COMPLETED, projectService.findById(userId, projectId).getStatus());
    }

    @Test
    public void finishByIndexTest() {
        Assert.assertNotNull(userId);
        projectRepository.finishByIndex(userId, 0);
        Assert.assertEquals(Status.COMPLETED, projectRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void finishByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.finishByName(userId, projectName);
        Assert.assertEquals(Status.COMPLETED, projectService.findByName(userId, projectName).getStatus());
    }

    @Test
    public void changeStatusByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.changeStatusById(userId, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findById(userId, projectId).getStatus());
        projectService.changeStatusById(userId, projectId, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findById(userId, projectId).getStatus());
        projectService.changeStatusById(userId, projectId, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectService.findById(userId, projectId).getStatus());
    }

    @Test
    public void changeStatusByIndexTest() {
        Assert.assertNotNull(userId);
        projectService.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByIndex(userId, 0).getStatus());
        projectService.changeStatusByIndex(userId, 0, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findByIndex(userId, 0).getStatus());
        projectService.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectService.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void changeStatusByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.changeStatusByName(userId, projectName, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByName(userId, projectName).getStatus());
        projectService.changeStatusByName(userId, projectName, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findByName(userId, projectName).getStatus());
        projectService.changeStatusByName(userId, projectName, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectService.findByName(userId, projectName).getStatus());
    }

}
