package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kyurinova.tm.api.repository.ISessionRepository;
import ru.tsc.kyurinova.tm.model.Session;
import ru.tsc.kyurinova.tm.model.User;

public class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final Session session;

    @NotNull
    private final String sessionId;

    @NotNull
    private final String userId;

    public SessionRepositoryTest() {
        session = new Session();
        sessionId = session.getId();
        @NotNull User user = new User();
        userId = user.getId();
    }

    @Before
    public void before() {
        session.setUserId(userId);
    }

    @Test
    public void existsSessionTest() {
        sessionRepository.add(session);
        Assert.assertTrue(sessionRepository.exists(sessionId));
    }

    @Test
    public void removeSessionByIdTest() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(sessionId);
        sessionRepository.removeById(sessionId);
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @After
    public void after() {
        sessionRepository.clear();
    }

}
